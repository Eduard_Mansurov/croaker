# Croaker
---

![](https://eduardprojects.me/java/croaker/favicon_150x150.ico)



**Croaker** is a social network where you can blogging, answer on posts (croaks) and copy them to your page (recroaks).


Word Croaker based on word croak which means sound a frog makes.

This project uses the following technologies:

+ Spring boot
+ Spring mvc
+ Spring security
+ Thymeleaf
+ Maven
+ Postgres
+ Supports REST API commands (https://eduardprojects.me/java/croaker/API)
+ Ajax
+ Jquery
+ Own CSS styles have been created
+ Project's architecture was build based on MVC pattern.


There are two roles: User and Administrator. User can create posts (also edit and remove them), answer on someone else's posts, copy someone else's posts to own page (recroak). Administrator has same abilities and in addition he can watch evey post (even private and close) and also he can remove any post.

User can set access settings to his own croaks:

+ Public - visible to every user
+ Private - user can choose those to whom the croak will be visible
+ Hidden - nobody can view thr croak except croak owner

This project deployed on virtual linux server and you can try it following the link eduardprojects.me/java/croaker (It uses HTTPS protocol)