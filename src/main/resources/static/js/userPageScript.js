;
$(document).ready(function () {
    var csrf = $('input[name=_csrf]').val();

    $(".select").dropdown({"autoinit": ".select"});

    $('body').on('click', '.dropdown input', function () {
        $('li[value=private]').attr("data-toggle", "modal").attr("data-target", "#complete-dialog"
            + $(this).parent('private').attr('croakId'));
    }).on('click', 'li', function () {
        if (this.textContent !== "Private croak" && this.textContent !== "Public croak" && this.textContent !== "Hidden croak" )
            return;
        var parent = $(this).parent().parent().parent();
        var id = $(parent).attr('croakid');
        var url;
        var access = $(this).attr('value');
        if ($(parent).attr('messagetype') === 'croak') {
            url = "changeCroakAccess";
        } else {
            url = "changeRecroakAccess";
        }
        if (access === "private") {
            $('.privateButton[croakid=' + id + ']').attr('style', 'display: inline-block;');
        } else {
            $('.privateButton[croakid=' + id + ']').attr('style', 'display: none;');
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {"id": id, "access": access, "_csrf": csrf},
            success: function () {
            },
            error: function (data) {
                console.log(data);
                errorDefault();
            }

        });
    }).on('click', '#submitButton', function (e) {
        e.preventDefault();
        var text = $('#croakInput').val();
        var location = $('#locationInput').val();
        var id = $(this).val();
        var access = "open";
        if (text != null && text !== ''
            && text.replace(/\s{2,}/g, ' ') !== ' ') {
            $.ajax({
                type: "POST",
                url: "newCroak",
                data: {"userId": id, "location": location, "croakText": text, "access": access, "_csrf": csrf},
                success: function (data) {
                    $(data).addClass("147");
                    $('.teste1').prepend(data);
                    $('#croakInput').val('').addClass('empty');
                    $('#locationInput').val('').addClass('empty');
                },
                error: function (data) {
                    console.log(data);
                    errorDefault()
                }
            });
        } else {
            $('#errorText').html("<h4><strong>Croak is empty!</strong></h4>");
        }
    }).on('click', '.deleteButton', function () {
        var id = $(this).attr('croakid') + "";
        var url;
        if ($(this).hasClass('Croak')) {
            url = "deleteCroak";
        } else {
            url = "deleteRecroak";
        }
        $.ajax({
            type: "POST",
            url: url,
            // context: this,
            data: {"id": id, "_csrf": csrf},
            success: function () {
                $('.croak' + id).remove();
            },
            error: function (data) {
                console.log(data);
                errorDefault()
            }
        });
    }).on('click', '.replyButton', function () {
        $('.answerButton').attr('value', $(this).attr('croakid'));
    }).on('click', '.answerButton', function () {
        var text = $('#answerInput').val();
        var id = $(this).val();
        var flag = false;
        var access = "open";
        $.ajax({
            type: "POST",
            url: "newRecroak",
            data: {"croakId": id, "answerText": text, "access": access, "RecroakOrAnswer": flag, "_csrf": csrf},
            success: function (data) {
                message("You answered")
            },
            error: function (data) {
                console.log(data);
                errorDefault()
            }
        });
    }).on('click', '.editButton', function () {
        var id = $(this).attr('croakid') + "";
        var textCroak = $('.croak' + id + ' .textCroak').text().trim();
        var locationCroak = $('.croak' + id + ' .croakLocation').text();
        var classPrev = $('#submitButton').attr('class');
        $('#croakInput').val(textCroak).removeClass('empty');
        $('#locationInput').val(locationCroak);
        if (locationCroak != '' && locationCroak != null) {
            $('#locationInput').removeClass('empty');
        }
        $('#submitButton').attr('id', 'editSubmit');
        $('#editSubmit').attr('class', classPrev + " " + id);
        $('#editSubmit').text('edit');
    }).on('click', '#editSubmit', function (e) {
        e.preventDefault();
        var textNEW = $('#croakInput').val();
        var locationNew = $('#locationInput').val();
        var croakID = $(this).attr('class').substring(27);
        var id = $('#editSubmit').val();
        var datestr = $('.croak' + croakID + ' .date').text().substring(119, 132);
        var classPrev = "btn btn-primary btn-raised";
        var access = $('.croak' + croakID + ' li[selected=selected]').val();
        $.ajax({
            type: "POST",
            url: "editCroak",
            data: {
                "id": croakID, "userId": id, "date": datestr, "location": locationNew,
                "croakText": textNEW, "access": access, "_csrf": csrf
            },
            success: function () {
                $('.croak' + croakID + ' .croakLocation').html(locationNew);
                $('.croak' + croakID + ' .textCroak').html(textNEW);
                $('#editSubmit').text('submit').attr('id', 'submitButton');
                $('#submitButton').attr('class', classPrev);
                $('#croakInput').val('').addClass('empty');
                $('#locationInput').val('').addClass('empty');
            },
            error: function (data) {
                console.log(data);
                errorDefault();
            }
        });
    }).on('click', '.editAccess', function () {
        var idCroak = $(this).attr('croakid');
        var setOfUsers = $('.newAccess' + idCroak).val();
        var type = $(this).attr('messageType');
        $.ajax({
            type: "POST",
            url: "accessEdit",
            data: {"id": idCroak, "list": setOfUsers, "type": type, "_csrf": csrf},
            success: function () {

            },
            error: function () {
                console.log();
                errorDefault();
            }

        });
    });

    $('.recroakButton').on('click', function () {
        var id = $(this).attr('croakid');
        var flag = true;
        var access = "open";
        $.ajax({
            type: "POST",
            url: "newRecroak",
            data: {"croakId": id, "RecroakOrAnswer": flag, "access": access, "_csrf": csrf},
            success: function () {
                message("Success recroacked");
            },
            error: function (data) {
                console.log(data);
                errorDefault();
            }
        });
    });

    $('.guestRecroakButton, .guestReplyButton').on('click', function () {
        window.location.href = "/java/croaker/login?unregisteredMessage";
    });

    $('#loginButton').on('click', function () {
        window.location.href = "/java/croaker/login";
    });

    function message(text, timeout) {
        if (!timeout && timeout != 0) {
            timeout = 2000; // default timeout 2 seconds
        }
        $.snackbar({
            content: text, // text of the snackbar
            timeout: timeout, // time in milliseconds after the snackbar autohides, 0 is disabled
            htmlAllowed: true // allows HTML as content value
        });
    }

    function errorDefault() {
        message("An error occurred while performing operation", 3000);
    }

});

