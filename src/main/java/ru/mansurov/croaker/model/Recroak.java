package ru.mansurov.croaker.model;

import ru.mansurov.croaker.utils.Helper;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "recroak_table")
public class Recroak extends CroakAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "croak")
    private Croak croak;
    @Column(name = "date_recroak")
    private Long date;
    @Column(name = "recroak_or_answer")
    private boolean flag;
    @Column(name = "answer_text")
    private String answerText;
    @Column(name = "access")
    private String access;

    @ManyToMany
    @JoinTable(
            name = "accessRecroak",
            joinColumns = @JoinColumn(name = "recroak_id"),
            inverseJoinColumns = @JoinColumn(name = "accessusers_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"recroak_id", "accessusers_id"}))
    private List<User> accessUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Croak getCroak() {
        return croak;
    }

    public void setCroak(Croak croak) {
        this.croak = croak;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }

    // convert list of users which have access into string with comma delimiter
    public String getAccessUsersString() {
        return Helper.getAccessUsersString(getAccessUsers());
    }
}
