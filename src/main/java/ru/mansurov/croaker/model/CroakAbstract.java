package ru.mansurov.croaker.model;

import javax.persistence.*;

@MappedSuperclass
public abstract class CroakAbstract {
    public abstract Long getDate();
}
