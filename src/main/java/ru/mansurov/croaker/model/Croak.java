package ru.mansurov.croaker.model;

import ru.mansurov.croaker.utils.Helper;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "croak_table")
public class Croak extends CroakAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "croak_text")
    private String text;
    @Column(name = "date_croak")
    private Long date;
    @Column(name = "location_croak", nullable = false)
    private String location;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(name = "access", nullable = false)
    private String access;

    @ManyToMany
    @JoinTable(
            name = "accessCroak",
            joinColumns = @JoinColumn(name = "croak_id"),
            inverseJoinColumns = @JoinColumn(name = "accessusers_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"croak_id", "accessusers_id"}))
    private List<User> accessUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }

    // convert list of users which have access into string with comma delimiter
    public String getAccessUsersString() {
        return Helper.getAccessUsersString(getAccessUsers());
    }
}
