package ru.mansurov.croaker.model;

public enum Role {
    USER,
    ADMIN;

    public String getName() {
        if (this == USER)
            return "User";
        else
            return "ADMIN";
    }
}
