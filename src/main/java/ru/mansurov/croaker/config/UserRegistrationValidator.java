package ru.mansurov.croaker.config;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mansurov.croaker.model.User;

@Component
public class UserRegistrationValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "registrationError.emptyNameField");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationError.emptyPasswordField");
    }
}
