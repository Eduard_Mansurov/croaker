package ru.mansurov.croaker.dto;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CroakDTO {
    private Long id;
    private String text;
    private Long date;
    private String location;
    private User user;

    public Croak toCroak() {
        Croak croak = new Croak();
        croak.setId(id);
        croak.setText(text);
        croak.setDate(date);
        croak.setLocation(location);
        croak.setUser(user);

        return croak;
    }

    public static CroakDTO fromCroak(Croak croak) {
        CroakDTO croakDTO = new CroakDTO();
        croakDTO.setId(croak.getId());
        croakDTO.setText(croak.getText());
        croakDTO.setDate(croak.getDate());
        croakDTO.setLocation(croak.getLocation());
        croakDTO.setUser(croak.getUser());

        return croakDTO;
    }

    public static List<CroakDTO> fromCroaks(List<Croak> croaks) {
        return croaks.stream().map(CroakDTO::fromCroak).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
