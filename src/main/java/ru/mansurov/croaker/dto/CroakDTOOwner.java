package ru.mansurov.croaker.dto;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CroakDTOOwner extends CroakDTO {
    private String access;
    private List<User> accessUsers;

    public static CroakDTOOwner fromCroak(Croak croak) {
        CroakDTOOwner croakDTOOwner = new CroakDTOOwner();
        croakDTOOwner.setId(croak.getId());
        croakDTOOwner.setText(croak.getText());
        croakDTOOwner.setDate(croak.getDate());
        croakDTOOwner.setLocation(croak.getLocation());
        croakDTOOwner.setUser(croak.getUser());
        croakDTOOwner.setAccess(croak.getAccess());
        croakDTOOwner.setAccessUsers(croak.getAccessUsers());

        return croakDTOOwner;
    }

    public static List<CroakDTOOwner> fromCroaksOwner(List<Croak> croaks) {
        return croaks.stream().map(CroakDTOOwner::fromCroak).collect(Collectors.toList());
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }
}
