package ru.mansurov.croaker.dto;

import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import java.util.ArrayList;
import java.util.List;

public class RecroakDTOOwner extends RecroakDTO {
    private String access;
    private List<User> accessUsers;

    public static RecroakDTOOwner fromRecroak(Recroak recroak) {
        RecroakDTOOwner recroakDTOOwner = new RecroakDTOOwner();
        recroakDTOOwner.setId(recroak.getId());
        recroakDTOOwner.setUser(recroak.getUser());
        recroakDTOOwner.setCroakDTO(CroakDTO.fromCroak(recroak.getCroak()));
        recroakDTOOwner.setDate(recroak.getDate());
        recroakDTOOwner.setFlag(recroak.isFlag());
        recroakDTOOwner.setAnswerText(recroak.getAnswerText());
        recroakDTOOwner.setAccess(recroak.getAccess());
        recroakDTOOwner.setAccessUsers(recroak.getAccessUsers());

        return recroakDTOOwner;
    }

    public static List<RecroakDTOOwner> fromRecroaksOwner(List<Recroak> recroaks) {
        List<RecroakDTOOwner> recroaksConverted = new ArrayList<>();
        for (Recroak recroak :
                recroaks) {
            recroaksConverted.add(RecroakDTOOwner.fromRecroak(recroak));
        }
        return recroaksConverted;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public List<User> getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(List<User> accessUsers) {
        this.accessUsers = accessUsers;
    }
}
