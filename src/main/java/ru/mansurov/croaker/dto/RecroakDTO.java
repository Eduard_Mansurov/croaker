package ru.mansurov.croaker.dto;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import java.util.ArrayList;
import java.util.List;

public class RecroakDTO {
    private Long id;
    private User user;
//    private Croak croak;
    private CroakDTO croakDTO;
    private Long date;
    private boolean flag;
    private String answerText;

    public static RecroakDTO fromRecroak(Recroak recroak) {
        RecroakDTO recroakDTO = new RecroakDTO();
        recroakDTO.setId(recroak.getId());
        recroakDTO.setUser(recroak.getUser());
//        recroakDTO.setCroak(recroak.getCroak());
        recroakDTO.setCroakDTO(CroakDTO.fromCroak(recroak.getCroak()));
        recroakDTO.setDate(recroak.getDate());
        recroakDTO.setFlag(recroak.isFlag());
        recroakDTO.setAnswerText(recroak.getAnswerText());

        return recroakDTO;
    }

    public static List<RecroakDTO> fromRecroaks(List<Recroak> recroaks) {
        List<RecroakDTO> croaksConverted = new ArrayList<>();
        for (Recroak recroak :
                recroaks) {
            croaksConverted.add(RecroakDTO.fromRecroak(recroak));
        }
        return croaksConverted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

//    public Croak getCroak() {
//        return croak;
//    }
//
//    public void setCroak(Croak croak) {
//        this.croak = croak;
//    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public CroakDTO getCroakDTO() {
        return croakDTO;
    }

    public void setCroakDTO(CroakDTO croakDTO) {
        this.croakDTO = croakDTO;
    }
}
