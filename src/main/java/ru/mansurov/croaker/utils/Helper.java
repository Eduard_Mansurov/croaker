package ru.mansurov.croaker.utils;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.CroakAbstract;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Helper {

    //method for merge list of Croaks and Recroaks and then sort them by date (Desc)
    public static List<Object> sort(List<Croak> croaks, List<Recroak> recroaks) {
        return Stream.of(croaks, recroaks)
                .flatMap(Collection::stream)
                .sorted(Comparator.comparingLong(CroakAbstract::getDate).reversed())
                .collect(Collectors.toList());
    }

    // convert list of users which have access into string with comma delimiter
    public static String getAccessUsersString(List<User> userListWithAccess) {
        return userListWithAccess.stream()
                .map(User::getUsername)
                .collect(Collectors.joining(", "));
    }
}
