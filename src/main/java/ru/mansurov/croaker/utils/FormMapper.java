package ru.mansurov.croaker.utils;

import ru.mansurov.croaker.form.CroakForm;
import ru.mansurov.croaker.form.RecroakForm;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * this class is for convert forms to model and vice versa
 */
public class FormMapper {

    public static CroakForm croakToCroakForm(Croak croak) {
        Date date = new Date(croak.getDate());
        CroakForm croakForm = new CroakForm();
        croakForm.setId(croak.getId());
        croakForm.setDateCroak(date);
        croakForm.setCroakText(croak.getText());
        croakForm.setLocation(croak.getLocation());
        croakForm.setUserId(croak.getUser().getId());
        String users = croak.getAccessUsers().stream()
                .map(User::getUsername)
                .collect(Collectors.joining(","));
        croakForm.setAccessUsers(users);
        croakForm.setAccess(croak.getAccess());
        return croakForm;
    }

    public static Croak croakFormToCroak(CroakForm croakForm, User user, List<User> list) {
        Croak croak = new Croak();
        if (croakForm.getId() != null) {
            croak.setId(croakForm.getId());
        }
        croak.setLocation(croakForm.getLocation());
        if (croakForm.getDateCroak() != null)
            croak.setDate(croakForm.getDateCroak().getTime());
        croak.setText(croakForm.getCroakText());
        croak.setUser(user);
        croak.setAccess(croakForm.getAccess());
        croak.setAccessUsers(list);
        return croak;
    }

    public static Recroak recroakFormToRecroak(RecroakForm recroakForm, Croak croak, User user, List<User> list) {
        Recroak recroak = new Recroak();
        if (recroakForm.getId() != null) {
            recroak.setId(recroakForm.getId());
        }
        if (recroakForm.getDateRecroak() != null)
            recroak.setDate(recroakForm.getDateRecroak().getTime());
        recroak.setCroak(croak);
        recroak.setUser(user);
        recroak.setFlag(recroakForm.isRecroakOrAnswer());
        recroak.setAnswerText(recroakForm.getAnswerText());

        recroak.setAccess(recroakForm.getAccess());
        recroak.setAccessUsers(list);
        return recroak;
    }

}
