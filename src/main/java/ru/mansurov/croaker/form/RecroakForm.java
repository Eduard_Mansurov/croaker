package ru.mansurov.croaker.form;

import java.util.Date;

public class RecroakForm {
    private Long id;
    private Long croakId;
    private boolean RecroakOrAnswer;
    private Date dateRecroak;
    private String answerText;
    private String access;
    private String accessUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCroakId() {
        return croakId;
    }

    public void setCroakId(Long croakId) {
        this.croakId = croakId;
    }

    public boolean isRecroakOrAnswer() {
        return RecroakOrAnswer;
    }

    public void setRecroakOrAnswer(boolean recroakOrAnswer) {
        RecroakOrAnswer = recroakOrAnswer;
    }

    public Date getDateRecroak() {
        return dateRecroak;
    }

    public void setDateRecroak(Date dateRecroak) {
        this.dateRecroak = dateRecroak;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(String accessUsers) {
        this.accessUsers = accessUsers;
    }
}
