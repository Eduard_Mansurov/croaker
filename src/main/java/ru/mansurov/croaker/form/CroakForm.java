package ru.mansurov.croaker.form;

import java.util.Date;

public class CroakForm {
    private Long id;
    private Long userId;
    private Date dateCroak;
    private String location;
    private String croakText;
    private String access;
    private String accessUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getDateCroak() {
        return dateCroak;
    }

    public void setDateCroak(Date dateCroak) {
        this.dateCroak = dateCroak;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCroakText() {
        return croakText;
    }

    public void setCroakText(String croakText) {
        this.croakText = croakText;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAccessUsers() {
        return accessUsers;
    }

    public void setAccessUsers(String accessUsers) {
        this.accessUsers = accessUsers;
    }
}