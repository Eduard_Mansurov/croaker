package ru.mansurov.croaker.service;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import java.util.List;

public interface RecroakService {
    List<Recroak> findByUserOrderByDateDesc(User pageOwner);

    List<Recroak> getPermittedRecroaks(User pageOwner, User user);

    void deleteRecroaksByCroak(Croak croak);

    void save(Recroak recroak);

    void deleteById(Long id);

    Recroak findById(Long id);

}
