package ru.mansurov.croaker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.repos.CroakRepo;
import ru.mansurov.croaker.service.CroakService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CroakServiceImpl implements CroakService {

    @Autowired
    CroakRepo croakRepo;

    @Override
    public List<Croak> findByUserOrderByDateDesc(User pageOwner) {
        return croakRepo.findByUserOrderByDateDesc(pageOwner);
    }

    @Override
    public List<Croak> getPermittedCroaks(User pageOwner, User user) {
        if (user != null && (user.getUsername().equals(pageOwner.getUsername()) || user.getRoles().contains(Role.ADMIN))) {
            return croakRepo.findByUserOrderByDateDesc(pageOwner);
        } else {
            return user != null ? croakRepo.getPermittedCroaks(pageOwner, user) :
                    croakRepo.getPermittedCroaks(pageOwner);
        }
    }

    @Override
    public void save(Croak croak) {
        // set current date for this new Croak
        if (croak.getDate() == null)
            croak.setDate(new Date().getTime());
        croakRepo.save(croak);
    }

    @Override
    public Croak findById(Long id) {
        Optional<Croak> croak = croakRepo.findById(id);
        return croak.orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        croakRepo.deleteById(id);
    }
}
