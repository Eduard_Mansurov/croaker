package ru.mansurov.croaker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.repos.RecroakRepo;
import ru.mansurov.croaker.service.RecroakService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RecroakServiceImpl implements RecroakService {
    @Autowired
    RecroakRepo recroakRepo;

    @Override
    public List<Recroak> findByUserOrderByDateDesc(User pageOwner) {
        return recroakRepo.findByUserOrderByDateDesc(pageOwner);
    }

    @Override
    public List<Recroak> getPermittedRecroaks(User pageOwner, User user) {
        if (user != null && (user.getUsername().equals(pageOwner.getUsername()) || user.getRoles().contains(Role.ADMIN))) {
            return recroakRepo.findByUserOrderByDateDesc(pageOwner);
        } else {
            return user != null ? recroakRepo.getPermittedRecroaks(pageOwner, user) :
                    recroakRepo.getPermittedRecroaks(pageOwner);
        }
    }

    @Override
    public void deleteRecroaksByCroak(Croak croak) {
        recroakRepo.deleteRecroaksByCroak(croak);
    }

    @Override
    public void save(Recroak recroak) {
        // set current date for this new Croak
        if (recroak.getDate() == null)
            recroak.setDate(new Date().getTime());
        recroakRepo.save(recroak);
    }

    @Override
    public void deleteById(Long id) {
        recroakRepo.deleteById(id);
    }

    @Override
    public Recroak findById(Long id) {
        Optional<Recroak> recroak = recroakRepo.findById(id);
        return recroak.orElse(null);
    }
}
