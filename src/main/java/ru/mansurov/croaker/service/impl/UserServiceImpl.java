package ru.mansurov.croaker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.repos.UserRepo;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserDetailsService, ru.mansurov.croaker.service.UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if (user != null) return user;
        else {
            throw new UsernameNotFoundException("User not found.");
        }
    }

    public boolean addUser(User user) {

        try {
            // set Role USER for all new users
            user.setRoles(new HashSet<Role>(Collections.singletonList(Role.USER)));
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepo.save(user);
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public List<User> findUsersByUsername(String[] users) {
        return userRepo.findUsersByUsernameIn(users);
    }

    @Override
    public List<User> findAllByOrderByUsername() {
        return userRepo.findAllByOrderByUsernameAsc();
    }
}
