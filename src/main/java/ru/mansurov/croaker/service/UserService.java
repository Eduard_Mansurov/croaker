package ru.mansurov.croaker.service;

import ru.mansurov.croaker.model.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);

    List<User> findUsersByUsername(String[] users);

    List<User> findAllByOrderByUsername();

}
