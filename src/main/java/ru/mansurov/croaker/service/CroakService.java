package ru.mansurov.croaker.service;

import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.User;

import java.util.List;

public interface CroakService {

    List<Croak> findByUserOrderByDateDesc(User pageOwner);

    List<Croak> getPermittedCroaks(User pageOwner, User user);

    void save(Croak croak);

    Croak findById(Long id);

    void deleteById(Long id);

}
