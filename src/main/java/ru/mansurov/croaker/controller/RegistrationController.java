package ru.mansurov.croaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.mansurov.croaker.config.UserRegistrationValidator;
import ru.mansurov.croaker.form.UserForm;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.UserServiceImpl;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    private UserRegistrationValidator validator;

    @GetMapping("/registrationPage")
    public String registrationPage(Model model) {
        model.addAttribute("UserForm", new UserForm());
        return "registrationPage";
    }

    @PostMapping("/addUser")
    public String addUser(User user,
                          @Valid @ModelAttribute("UserForm") UserForm userForm,
                          BindingResult bindingResult) {

        // check form for correctness
        validator.validate(userForm, bindingResult);

        boolean userCreation;

        if (bindingResult.hasErrors())
            return "registrationPage";

        userCreation = userServiceImpl.addUser(user);

        if (userCreation) {
            return "redirect:/";
        }
        return "redirect:/registrationPage?error";

    }
}
