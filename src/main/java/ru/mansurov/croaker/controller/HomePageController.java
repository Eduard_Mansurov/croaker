package ru.mansurov.croaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.UserServiceImpl;

@Controller
public class HomePageController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping
    public String homePage(@AuthenticationPrincipal User user,
                           Model model) {
        model.addAttribute("user", user);
        model.addAttribute("users", userServiceImpl.findAllByOrderByUsername());

        return "homePage";
    }

    @GetMapping("/currentUserPage")
    public String currentUserPage(@AuthenticationPrincipal User user) {
        return "redirect:/user/" + user.getUsername();
    }

    @GetMapping("/API")
    public String APIPage(@AuthenticationPrincipal User user,
                          Model model) {
        model.addAttribute("user", user);
        return "APIPage";
    }
}
