package ru.mansurov.croaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.mansurov.croaker.form.CroakForm;
import ru.mansurov.croaker.form.RecroakForm;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.CroakServiceImpl;
import ru.mansurov.croaker.service.impl.RecroakServiceImpl;
import ru.mansurov.croaker.service.impl.UserServiceImpl;
import ru.mansurov.croaker.utils.FormMapper;
import ru.mansurov.croaker.utils.Helper;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserPageController {

    @Autowired
    CroakServiceImpl croakService;
    @Autowired
    RecroakServiceImpl recroakService;
    @Autowired
    UserServiceImpl userServiceImpl;

    @GetMapping("/{userName}")
    public String homePageUser(@AuthenticationPrincipal User user,
                               @PathVariable("userName") String pageOwnerName,
                               Model model) {

        // page owner of page which we view
        User pageOwnerUser = userServiceImpl.findByUsername(pageOwnerName);

        if (pageOwnerUser != null) {

            List<Croak> listCroak;
            List<Recroak> listRecroak;

            // for page Owner get all his Croaks and Recroaks, for guest get only permitted
            // for him Croaks and Recroaks
            listCroak = croakService.getPermittedCroaks(pageOwnerUser, user);
            listRecroak = recroakService.getPermittedRecroaks(pageOwnerUser, user);

            if (user == null) {
                user = new User();
                user.setRoles(Collections.singleton(Role.USER));
            }

            model.addAttribute(user);
            model.addAttribute("pageOwner", pageOwnerUser);

            // if user does not have Croaks or Recroaks return page with empty list
            if (listCroak.isEmpty() && listRecroak.isEmpty()) return "/userPage";

            // sort Croaks and Recroaks by date and add to model
            model.addAttribute("croaksAndRecroaks", Helper.sort(listCroak, listRecroak));

            return "/userPage";
        } else {
            return "redirect:/" + (user != null ? "user/" + user.getUsername() : "");
        }
    }

    @PostMapping("/newCroak")
    public String newCroak(@ModelAttribute("croak") CroakForm croakForm,
                           @AuthenticationPrincipal User user,
                           Model model) {

        Croak croak = FormMapper.croakFormToCroak(croakForm, user, null);
        croakService.save(croak);
        model.addAttribute("croak", croak);
        model.addAttribute("user", user);
        model.addAttribute("pageOwner", user);
        return "fragments/croakFragment.html";
    }

    @PostMapping("/deleteCroak")
    @ResponseBody
    public Boolean deleteCroak(@RequestParam("id") String id,
                               @AuthenticationPrincipal User user) {

        Long idLong = Long.parseLong(id);
        if (croakService.findById(idLong).getUser().getId().equals(user.getId())) {
            // remove all Recroaks and Answers based on current croak
            recroakService.deleteRecroaksByCroak(croakService.findById(idLong));

            croakService.deleteById(idLong);
            return true;
        }

        return false;
    }

    @RequestMapping("/editCroak")
    @ResponseBody
    public Boolean editCroak(@ModelAttribute("croak") CroakForm croakForm,
                             @AuthenticationPrincipal User user) {

        // Edit croak's text and location
        if (croakService.findById(croakForm.getId()).getUser().getId().equals(user.getId())) {
            Croak croak = croakService.findById(croakForm.getId());
            croak.setText(croakForm.getCroakText());
            croak.setLocation(croakForm.getLocation());

            croakService.save(croak);
            return true;
        }

        return false;
    }

    @PostMapping("/newRecroak")
    @ResponseBody
    public Boolean recroak(@ModelAttribute("recroak") RecroakForm recroakForm,
                           @AuthenticationPrincipal User user) {
        recroakService.save(FormMapper.recroakFormToRecroak(recroakForm,
                croakService.findById(recroakForm.getCroakId()), user, null));
        return true;
    }

    @PostMapping("/deleteRecroak")
    @ResponseBody
    public Boolean deleteRecroak(@RequestParam("id") String id,
                                 @AuthenticationPrincipal User user) {

        Long idLong = Long.parseLong(id);
        if (recroakService.findById(idLong).getUser().getId().equals(user.getId())) {
            recroakService.deleteById(idLong);
            return true;
        }

        return false;
    }

    @PostMapping("/changeCroakAccess")
    @ResponseBody
    public void changeCroakAccess(@RequestParam("id") String id,
                                  @RequestParam("access") String access,
                                  @AuthenticationPrincipal User user) {

        Long idLong = Long.parseLong(id);
        Croak croak = croakService.findById(idLong);
        if (croak.getUser().getId().equals(user.getId())) {
            croak.setAccess(access);
            croakService.save(croak);
        }
    }

    @PostMapping("/changeRecroakAccess")
    @ResponseBody
    public void changeRecroakAccess(@RequestParam("id") String id,
                                    @RequestParam("access") String access,
                                    @AuthenticationPrincipal User user) {

        Recroak recroak = recroakService.findById(Long.parseLong(id));
        if (recroak.getUser().getId().equals(user.getId())) {
            recroak.setAccess(access);
            recroakService.save(recroak);
        }
    }

    @PostMapping("/accessEdit")
    @ResponseBody
    public void access(@RequestParam("type") String type,
                       @RequestParam("id") String id,
                       @RequestParam("list") String usersAccess,
                       @AuthenticationPrincipal User user) {

        String[] users = usersAccess.split(", ");
        List<User> list = userServiceImpl.findUsersByUsername(users);

        if (type.equals("croak")) {
            Croak croak = croakService.findById(Long.parseLong(id));
            if (croak.getUser().getId().equals(user.getId())) {
                croak.setAccessUsers(list);
                croakService.save(croak);
            }
        } else {
            Recroak recroak = recroakService.findById(Long.parseLong(id));
            if (recroak.getUser().getId().equals(user.getId())) {
                recroak.setAccessUsers(list);
                recroakService.save(recroak);
            }
        }
    }
}
