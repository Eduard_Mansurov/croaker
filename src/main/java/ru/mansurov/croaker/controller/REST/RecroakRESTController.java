package ru.mansurov.croaker.controller.REST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.mansurov.croaker.controller.REST.exceptions.BadRequest;
import ru.mansurov.croaker.controller.REST.exceptions.Forbidden;
import ru.mansurov.croaker.controller.REST.exceptions.NotFound;
import ru.mansurov.croaker.dto.CroakDTO;
import ru.mansurov.croaker.dto.RecroakDTO;
import ru.mansurov.croaker.dto.RecroakDTOOwner;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.CroakServiceImpl;
import ru.mansurov.croaker.service.impl.RecroakServiceImpl;
import ru.mansurov.croaker.service.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping(value = "/api/recroak", produces = {MediaType.APPLICATION_JSON_VALUE})
public class RecroakRESTController {
    @Autowired
    RecroakServiceImpl recroakService;

    @Autowired
    CroakServiceImpl croakService;

    @Autowired
    UserServiceImpl userServiceImpl;

    @PostMapping("/createRecroak")
    public RecroakDTOOwner createRecroak(@RequestBody Croak croak,
                                         @AuthenticationPrincipal User user) {

        return createRecroakOrAnswer(croak, user, false);
    }

    @PostMapping("/createAnswer")
    public RecroakDTOOwner createAnswer(@RequestBody Croak croak,
                                        @AuthenticationPrincipal User user) {

        return createRecroakOrAnswer(croak, user, true);
    }

    private RecroakDTOOwner createRecroakOrAnswer(Croak croak, User user, boolean isAnswer) {
        Croak croakedCroak = croakService.findById(croak.getId());
        if (croakedCroak == null)
            throw new NotFound("Croak for answer was not found");

        List<Croak> croaks = croakService.getPermittedCroaks(croakedCroak.getUser(), user);
        if (!croaks.contains(croakedCroak))
            throw new Forbidden("Access denied");

        if (croak.getText() == null || croak.getText().isEmpty()) {
            throw new BadRequest("Text field is empty");
        }

        Recroak recroak = new Recroak();
        recroak.setUser(user);
        recroak.setCroak(croakedCroak);
        recroak.setAccess("open");

        if (isAnswer) {
            recroak.setAnswerText(croak.getText());
            recroak.setFlag(false);
        } else {
            recroak.setAnswerText("");
            recroak.setFlag(true);
        }

        recroakService.save(recroak);

        RecroakDTOOwner recroakDTOOwner = RecroakDTOOwner.fromRecroak(recroak);
        recroakDTOOwner.setCroakDTO(CroakDTO.fromCroak(croakedCroak));

        return recroakDTOOwner;
    }

    @GetMapping("/recroaks/{userName}")
    public List<? extends RecroakDTO> getCroaksByUser(@PathVariable("userName") String userName,
                                                      @AuthenticationPrincipal User user) {

        User pageOwnerUser = userServiceImpl.findByUsername(userName);
        List<Recroak> recroaks = recroakService.getPermittedRecroaks(pageOwnerUser, user);
        List<? extends RecroakDTO> recroaksConverted;
        if (user != null && (user.getUsername().equals(userName) || user.getRoles().contains(Role.ADMIN))) {
            recroaksConverted = RecroakDTOOwner.fromRecroaksOwner(recroaks);
        } else {
            recroaksConverted = RecroakDTO.fromRecroaks(recroaks);
        }

        return recroaksConverted;
    }

    @DeleteMapping("/deleteRecroak/{id}")
    public void deleteRecroak(@PathVariable("id") String id,
                              @AuthenticationPrincipal User user) {

        Recroak recroak = recroakService.findById(Long.parseLong(id));
        if (recroak.getUser().getId().equals(user.getId()))
            recroakService.deleteById(recroak.getId());
        else
            throw new Forbidden("Access denied");

    }
}
