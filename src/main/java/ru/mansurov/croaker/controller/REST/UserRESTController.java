package ru.mansurov.croaker.controller.REST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import ru.mansurov.croaker.controller.REST.exceptions.BadRequest;
import ru.mansurov.croaker.controller.REST.exceptions.Unauthorized;
import ru.mansurov.croaker.controller.REST.exceptions.UserAlreadyExists;
import ru.mansurov.croaker.dto.AuthenticationRequestDto;
import ru.mansurov.croaker.jwtsecurity.JwtTokenProvider;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.UserServiceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/user", produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserRESTController {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.findAllByOrderByUsername();
    }

    @PostMapping("/createUser")
    public User createUser(@RequestBody User newUser) {

        String newUserUsername = newUser.getUsername();
        String newUserPassword = newUser.getPassword();
        if (newUserUsername == null || newUserUsername.isEmpty())
            throw new BadRequest("Username field is empty");
        if (newUserPassword == null || newUserPassword.isEmpty())
            throw new BadRequest("Password field is empty");
        if (!userService.addUser(newUser))
            throw new UserAlreadyExists();

        return userService.findByUsername(newUserUsername);
    }

    @PostMapping("login")
    public ResponseEntity login(@RequestBody AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();
            if (username == null || username.isEmpty())
                throw new BadRequest("Username field is empty");
            String password = requestDto.getPassword();
            if (password == null || password.isEmpty())
                throw new BadRequest("Password field is empty");

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            User user = userService.findByUsername(username);

            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            String token = jwtTokenProvider.createToken(username, user.getRoles());

            Map<Object, Object> response = new HashMap<>();
            response.put("username", username);
            response.put("token", token);

            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new Unauthorized();
        }
    }

}
