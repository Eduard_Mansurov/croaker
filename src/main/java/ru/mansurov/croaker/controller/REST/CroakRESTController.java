package ru.mansurov.croaker.controller.REST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.mansurov.croaker.controller.REST.exceptions.BadRequest;
import ru.mansurov.croaker.controller.REST.exceptions.Forbidden;
import ru.mansurov.croaker.controller.REST.exceptions.NotFound;
import ru.mansurov.croaker.dto.CroakDTO;
import ru.mansurov.croaker.dto.CroakDTOOwner;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Role;
import ru.mansurov.croaker.model.User;
import ru.mansurov.croaker.service.impl.CroakServiceImpl;
import ru.mansurov.croaker.service.impl.RecroakServiceImpl;
import ru.mansurov.croaker.service.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping(value = "/api/croak", produces = {MediaType.APPLICATION_JSON_VALUE})
public class CroakRESTController {
    @Autowired
    CroakServiceImpl croakService;

    @Autowired
    RecroakServiceImpl recroakService;

    @Autowired
    UserServiceImpl userServiceImpl;

    @PostMapping("/createCroak")
    public Croak createCroak(@RequestBody Croak croak,
                             @AuthenticationPrincipal User user) {

        String croakText = croak.getText();
        if (croakText == null || croakText.isEmpty())
            throw new BadRequest("Text field is empty");
        if (croak.getLocation() == null)
            croak.setLocation("");
        croak.setAccess("open");
        croak.setUser(user);
        croakService.save(croak);

        return croakService.findById(croak.getId());
    }

    @DeleteMapping("/deleteCroak/{id}")
    public void deleteCroak(@PathVariable("id") String id,
                            @AuthenticationPrincipal User user) {

        Croak croak = croakService.findById(Long.parseLong(id));
        if (croak.getUser().getId().equals(user.getId())) {
            // remove all Recroaks and Answers based on current croak
            recroakService.deleteRecroaksByCroak(croakService.findById(croak.getId()));

            croakService.deleteById(croak.getId());
        } else {
            throw new Forbidden("Access denied");
        }
    }

    @GetMapping("/croaks/{userName}")
    public List<? extends CroakDTO> getCroaksByUser(@PathVariable("userName") String userName,
                                                    @AuthenticationPrincipal User user) {

        User pageOwnerUser = userServiceImpl.findByUsername(userName);
        if (pageOwnerUser == null)
            throw new NotFound("User not found");

        List<Croak> croaks = croakService.getPermittedCroaks(pageOwnerUser, user);
        List<? extends CroakDTO> croaksConverted;
        if (user != null && (user.getUsername().equals(userName) || user.getRoles().contains(Role.ADMIN))) {
            croaksConverted = CroakDTOOwner.fromCroaksOwner(croaks);
        } else {
            croaksConverted = CroakDTO.fromCroaks(croaks);
        }

        return croaksConverted;
    }

    @PutMapping("/editCroak/{id}")
    public Croak editCroak(@PathVariable("id") String id,
                           @AuthenticationPrincipal User user,
                           @RequestBody Croak croak) {

        String croakText = croak.getText();
        if (croakText == null || croakText.isEmpty())
            throw new BadRequest("Text field is empty");

        Croak DBCroak = croakService.findById(Long.parseLong(id));
        if (DBCroak.getUser().getId().equals(user.getId())) {
            DBCroak.setText(croak.getText());
            DBCroak.setLocation(croak.getLocation());
            croakService.save(DBCroak);
        } else {
            throw new Forbidden("Access denied");
        }

        return DBCroak;
    }
}
