package ru.mansurov.croaker.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.Recroak;
import ru.mansurov.croaker.model.User;

import javax.transaction.Transactional;
import java.util.List;

public interface RecroakRepo extends JpaRepository<Recroak, Long> {

    List<Recroak> findByUserOrderByDateDesc(User pageOwner);

    @Query(value = "(SELECT rt.* " +
            "from recroak_table rt join access_recroak ar ON rt.id = ar.recroak_id " +
            "WHERE ar.accessusers_id = ?2 and rt.access = 'private' and rt.user_id = ?1 " +
            "union all (SELECT * FROM recroak_table rtu WHERE access = 'open' and rtu.user_id = ?1)) order by date_recroak DESC",
            nativeQuery = true)
    List<Recroak> getPermittedRecroaks(User pageOwner, User user);

    @Transactional
    void deleteRecroaksByCroak(Croak croak);

    @Query(value = "SELECT * FROM recroak_table rtu WHERE access = 'open' and rtu.user_id = ?1 order by date_recroak DESC",
            nativeQuery = true)
    List<Recroak> getPermittedRecroaks(User pageOwner);
}
