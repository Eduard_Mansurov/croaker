package ru.mansurov.croaker.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.mansurov.croaker.model.Croak;
import ru.mansurov.croaker.model.User;

import java.util.List;

public interface CroakRepo extends JpaRepository<Croak, Long> {

    void deleteById(Long id);

    List<Croak> findByUserOrderByDateDesc(User pageOwner);

    @Query(value = "(SELECT ct.* " +
            "from croak_table ct join access_croak ac ON ct.id = ac.croak_id " +
            "WHERE ac.accessusers_id = ?2 and ct.access = 'private' and ct.user_id = ?1 " +
            "union all (SELECT * FROM croak_table ctu WHERE access = 'open' and ctu.user_id = ?1)) order by date_croak DESC",
            nativeQuery = true)
    List<Croak> getPermittedCroaks(User pageOwner, User user);

    @Query(value = "SELECT * FROM croak_table ctu WHERE access = 'open' and ctu.user_id = ?1 order by date_croak DESC",
            nativeQuery = true)
    List<Croak> getPermittedCroaks(User pageOwner);
}
