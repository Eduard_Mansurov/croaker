package ru.mansurov.croaker.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.mansurov.croaker.model.User;

import java.util.List;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);

    List<User> findUsersByUsernameIn(String[] users);

    @Query(value = "SELECT * FROM user_table ORDER BY upper(username) ASC",
            nativeQuery = true)
    List<User> findAllByOrderByUsernameAsc();

}
